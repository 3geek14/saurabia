const fonts = ["Montserrat", "Arial", "Verdana", "Helvetica"];

function shuffleFonts() {
  const content = document.getElementsByClassName("content").item(0);
  const paragraphs = Array.from(content.getElementsByTagName("p"));
  const listItems = Array.from(content.getElementsByTagName("li"));

  paragraphs.concat(listItems).forEach((element) => {
    const fontIndex = Math.floor(Math.random() * fonts.length);
    element.style.fontFamily = fonts[fontIndex];
  });
}
window.addEventListener("load", shuffleFonts);

function addMargins() {
  const content = document.getElementsByClassName("content")[0];
  const paragraphs = Array.from(content.getElementsByTagName("p"));
  const listItems = Array.from(content.getElementsByTagName("li"));

  paragraphs.forEach((paragraph) => {
    const defaultLeftMargin = window
      .getComputedStyle(paragraph)
      .getPropertyValue("margin-left");
    const leftMarginScale = 1.5 * Math.random() + 0.5;

    const stringParts = defaultLeftMargin.match(/[a-zA-Z]+|[0-9\.-]+/g);
    if (stringParts.length == 2) {
      const quantity = Number(stringParts[0]);
      const unit = stringParts[1];
      paragraph.style.marginLeft = quantity * leftMarginScale + unit;
    }
  });

  listItems.forEach((listItem) => {
    const marginLeft = 1 * Math.random() - 0.5;
    listItem.style.marginLeft = marginLeft + "em";
  });
}
window.addEventListener("load", addMargins);

function changeLineHeights() {
  const content = document.getElementsByClassName("content").item(0);
  const paragraphs = Array.from(content.getElementsByTagName("p"));
  const listItems = Array.from(content.getElementsByTagName("li"));

  paragraphs.concat(listItems).forEach((element) => {
    const defaultLineHeight = window
      .getComputedStyle(element)
      .getPropertyValue("line-height");
    const lineHeightScale = 0.2 * Math.random() + 0.9;

    const stringParts = defaultLineHeight.match(/[a-zA-Z]+|[0-9\.-]+/g);
    if (stringParts.length == 2) {
      const quantity = Number(stringParts[0]);
      const unit = stringParts[1];
      element.style.lineHeight = quantity * lineHeightScale + unit;
    }
  });
}
window.addEventListener("load", changeLineHeights);
