function menuToggle() {
  var elems = document.getElementsByClassName("links");
  var menu = document.getElementsByClassName("menu");

  var icon = document.getElementById("menuicon");
  var width = document.documentElement.clientWidth;
  Array.from(elems).forEach((x) => {
    if (width < 768) {
      if (x.style.display === "block") {
        x.style.display = "none";
      } else {
        x.style.display = "block";
      }
    }
  });

  Array.from(menu).forEach((x) => {
    if (width < 768) {
      if (x.style.display === "grid") {
        x.style.display = "none";
      } else {
        x.style.display = "grid";
      }
    }
  });

  if (icon.getAttribute("src") == "../images/list.svg") {
    icon.src = "../images/x-lg.svg";
  } else {
    icon.src = "../images/list.svg";
  }
}
