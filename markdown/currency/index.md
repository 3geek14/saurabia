---
title: Saurabia - Currency
lang: en-CA
...

:::{.links}

<!-- * ## Side navigation bar goes here.
* [Link text](#target) -->

:::

:::{.content}

# Saurabiucks

Everyone's favourite currency in all of the Realms, it's Saurabiucks! Backed by
armour from our very own armoury, Saurabiucks are currently available in three
denominations for you to select from.

The 1.3

: The original. The classic. [Introduced in 1015](../history/#year-1015),
  this marvelous wooden nickel has our signature dinosaur head in line art on
  one side, and a large 1.3 in a friendly font on the other side.

The 1.8

: For lovers of slim wallets. [Introduced in 1016](../history/#year-1016),
  this luxurious coin is made from the finest of woods. A friendly 1.8 adorns
  one face, and the inspirational visage of our heraldry features prominently on
  the reverse.

The Tree Fiddy

: Gamer-friendly. Casino quality. [Introduced in 1023](../history/#year-1023),
  this orange die has a nice heft and feels good in the hand. The faces one
  through six display the value of the die, with our dinosaur head in place of
  the one, three trees in place of the three, and five fiddles in place of the
  five.

:::