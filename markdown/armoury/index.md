---
title: Saurabia - Armoury
lang: en-CA
...

:::{.links}

* ## [Background](#background)
* ## [New Prices](#new-prices)
* [Base Price](#base-price)
* [Gold Conversion](#gold-conversion)
* [Large Denominations](#large-denominations)
* [Regional Bonuses](#regional-bonuses)
* ## [Conclusion](#conclusion)

:::

:::{.content}

# Updates to Prices from the Saurabian Armory

\[Sorry for the OOC nature of this page. I initially wrote it to be a Discord
post.\]

## Background

For a long time, I've been charging 650 Saurabiucks for a basic chainmail shirt.
(Fiddling with colours can cost extra.) I've also been accepting gold from other
backers at a rate of 2 gold to 1 Saurabiuck (so 1300 gold if no Saurabiucks are
used).

In response to the rising OOC costs of materials and labour, I'm making a couple
of changes to this. I know that some people have been working for a while to
gather funds for a shirt, and I don't want to spring these changes on those
people. I think I've already informed the people I know about, and I've promised
to still let them buy at the old prices. If you're in this position and haven't
spoken to me about this, DM me and we can work it out.

## New Prices

### Base Price

A basic shirt will now cost 650 Saurabiucks. Changing the colour of rings will
cost an extra 130 Saurabiucks for either the aluminum or the rubber, and it'll
cost 260 Saurabiucks to change both.

(But wait, that sounds the same as the old price? So far, yeah.)

### Gold Conversion

Gold will still be accepted at 2 gold to 1 Saurabiuck. However, at most half of
the total purchase can be paid for in this way. For a basic shirt, that would
mean 650 gold and 325 Saurabiucks.

### Large Denominations

Additionally, I will be discounting high-value currencies. If something has a
nominal value of 5 or more, expect me to under-value it. The higher its nominal
value, the more I'll under-value it. In general, if a coin is nominally worth 5
of something else, I'll treat it as 4 of that other thing. I'll list a few
examples here, but I also expect to receive questions. 

* A Chimeronian rowan is nominally worth 5 monarchs. Since I treat their
  monarchs as 3 gold, I'll treat rowans as 12 gold. 
* A Blackwood black chip is nominally worth 5 yellow chips. Since I treat their
  yellow chips as 1 gold, I'll treat their black chips as 4 gold. 
* A Blackwood green chip is nominally worth 5 black chips. Since I treat their
  black chips as 4 gold, I'll treat their green chips as 16 gold.
* I'll treat gold coins of the Realms as 8 gold. My advisors told me this was
  simpler than "roughly 7.267 gold".

I have a few reasons for this, but the main one is that I'd rather have 10
1-gold coins than have a single 10-gold coin. Easier to give out as NPC drops
and tips, which are the two things I spend the most gold on. I don't often want
to spend a lot of gold in one place, so compact currencies don't really help me.

As an exception to this rule, lucky rolls of Saurabian Tree Fiddies will be
taken at face value. And any future denominations of Saurabiucks will also be
exempt from this.

### Regional Bonuses

If you pay me at a gathering within a nation that backs a currency, I'll accept
their currency with a 40% bonus. So at Feast of the Leviathan, rather than
accepting Leviathan scales as 4 gold, I'll accept them as 5.6 gold. (Still less
than the 10 gold Aeston will take them as, unfortunately.) Additionally, coins
backed by this nation won't count towards the 50% limit for gold. So, at Feast
of the Leviathan, you could buy a basic shirt for 200 Leviathan scales and 50 of
the 1.8 Saurabiuck coins. You could also buy a basic shirt for 650 silver coins
of the Realms, 100 Leviathan scales, and 25 of the 1.8 Saurabiuck coins.

There are a bunch of reasons for this as well. Though, the fact that I find it
amusing might be the most impactful one. 

## Conclusion

If you pay entirely in Saurabiucks, the price hasn't changed at all. You
(mostly) can no longer pay entirely in gold. Paying in high-value coins raises
the price. Paying with the local currency lowers the price.

Please feel free to reach out to me with questions.

:::