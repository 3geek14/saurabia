---
title: Saurabia - History
lang: en-CA
...

:::{.links}

* ## Jump to a year!
* [1013](#year-1013)
* [1014](#year-1014)
* [1015](#year-1015)
* [1016](#year-1016)
* [1019](#year-1019)
* [1020](#year-1020)
* [1022](#year-1022)
* [1023](#year-1023)
* [1024](#year-1024)

:::

:::{.content}

# Timeline of Events in Saurabia

There are many events which have happened but have not been recorded on this
page. If you have something you think we should include, please get in touch
with any of us, and we'll be more than happy to include that event!



[March 17, 1013]{#year-1013}

: At [The Dragon's Den II](https://www.realmsnet.net/events/1292/archive), in
  what wound up being an unfortunate way to irrevocably link dinosaurs to
  dragons in the minds of certain kings, Larrysaurus founded the glorious nation
  of Saurabia as our very first Rex.

April 1, 1013

: Professor Pipsilophicaphagus recognizes the power of propaganda and creates a
  media presence for the burgeoning nation.

September 8, 1013

: After a long and arduous media campaign, mapmakers around the Realms finally
  acknowledge Saurabia as a proper nation, drawing our borders in ink onto their
  maps.

October 31, 1013

: Dame Twenaria recognizes the support she can lend to our nation and commits to
  founding the Saurabia Fan Club. She plans to take the role of first President.

November 2, 1013

: At [Rhiassa Presents: What Lurks Beneath V](https://www.realmsnet.net/events/1241/archive),
  Rox Foulpuke follows Larrysaurus to return a dropped belt favour. Upon
  attempted return of the favour, our Rex gave the friendly goblin a sword and a
  suit of armour. Our trusting Rex also appoints Rox to be the nation's first
  Bankallosaurus. Aeston, always a stickler for spelling, would prefer the title
  be "Bankylosaurus".

November 3, 1013

: Sakariasvafnir "Saka" Keeneye, upset at his merciless king's refusal to permit
  dual-citizenship outside of the conglomerate, joins the Saurabia Fan Club. The
  Dread Pirate Kahlenar also sees the spark of joy, and he too joins. With three
  founding members, the Saurabia Fan Club is now official!

[March 15, 1014]{#year-1014}

: At [The Most Gruesome Donnybrook of Havoc and Mayhem (TM)](https://www.realmsnet.net/events/1326/archive),
  Larrysaurus and Comedus have a kid! Well, at the very least, Comedus lays an
  egg, and he blames this on Larrysaurus. Larrysaurus is a gentlemansaur who
  would never kiss and tell, so no further details are know. Larrysaurus agrees
  to take responsibility for the egg, walking around in circles in warm places
  to help it hatch.

December 13, 1014

: At [The Huntress Guild Presents: Tournaments of Artemis IV](https://www.realmsnet.net/events/1402/archive),
  Twenaria's familiar grows up! No longer bound to a small size, Alpacasaurus
  joins Saurabia as a full member.

[April 4, 1015]{#year-1015}

: Saurabia hosts an open house to give a tour of the volcano to the Realms. At
  the open house, sometimes called [Saurabian Questing](https://www.realmsnet.net/events/1396/archive),
  the Realms learns that monsters in the Underwhere are surprisingly wealthy in
  Saurabiucks. It's as if all the coins we lost in the couch cushions wound up
  down there.

August 16, 1015

: At [Rhiassa Presents: Queen of Hearts XXII](https://www.realmsnet.net/events/1454/archive),
  the seer Esmerelda of Chimeron foresees a future in which Saurabia will need
  new denominations of money. She helpfully provides the prototype for the
  [Tree Fiddy](../currency/).
  
  Team Hooray! wins overall Team Spirit, which concluded with Sir Tao getting the
  final kill for Team Hooray! to win Queen's War.

[April 9, 1016]{#year-1016}

: Saurabia hosts [Saurabian Tournaments II](https://www.realmsnet.net/events/1514/archive),
  an excellent opportunity to reveal the wisdom of their currency's
  denominations, where the competitors who place highly in each event receive
  small piles of Saurabiucks, including a new denomination.

[October 26, 1019]{#year-1019}

: Members of the Saurabia Undead Network travel across the Bifrost to Norlund,
  where their help is needed at [Rhiassa Presents: Echoes of Ragnarok V](https://www.realmsnet.net/events/1709/archive)
  to create new Asir for the lands. Tychasaurus, the Dinosaur god of Luck and
  Fun! Hooray! is born as the patron of Midgard.

[January 18, 1020]{#year-1020}

: During [Rhiassa Presents: Feast of the Leviathan XXII](https://www.realmsnet.net/events/1712/archive),
  members of Saurabia responsible for Tychasaurus lead her troops to victory in
  the first Asirwar. This victory grants her the right to decide what species
  will be irrevocably tied to the very core of Norlund, as trolls once were.
  This is why trolls used to be the species most enduring within Norlund, and
  why now dinosaurs hold that position. This works out very well for the local
  vikings, who get some sweet mounts and beasts of burden to help them rebuild.

[April 30, 1022]{#year-1022}

: Larrysaurus determines the time has come for him to expand his worldly
  horizons, and he begins his trip out west, far past the Western Flank. He
  passes the torch of leadership to Alpacasaurus, our new Rex.

October 22, 1022

: No longer content to participate through the Saurabia Fan Club, Dame Twenaria
  join Saurabia at [Black & White 2022](https://www.realmsnet.net/events/1854/archive).
  Her once familiar, now Rex, swears her in.

[May 13, 1023]{#year-1023}

: Debuting at [The Clontarf Shuffle III with Stacked Deck Final Table](https://www.realmsnet.net/events/1960/archive),
  a day of gambling is the perfect introduction for money made out of dice.

[November 2, 1024]{#year-1024}

: At the [Black & White 2024](https://www.realmsnet.net/events/1978/archive)
  Masquerade Ball, in his final Court as King of Chimeron, King Cecil Behind the
  Bar opens Court by announcing where Chimeron can look for leadership after he
  steps down. To nobody's surprise, he announced that Chimeron would be joining
  the Saurabian Empire, fulfilling Chimeron's true mission of being part of a
  nesting doll of nations. Just as every subnation of Chimeron has its own
  leader, Saurabia's newest subnation will be lead by future King Orion.

:::