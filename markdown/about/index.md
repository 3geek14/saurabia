---
title: Saurabia - About Us
lang: en-CA
...

:::{.links}

<!-- * ## Side navigation bar goes here.
* [Link text](#target) -->

:::

:::{.content}

# About Us

We are the nation of Saurabia. Different cultures within the Realms have been
described as being "northern" and "southern". As we are located south of the
Southern Wastes, it's safe to assume that, culturally, we are the Deep South of
the Realms.

We live in a volcano that was revealed near the end of the Bedlam Wars, and we
welcome guests to visit us in our guest volcano! We've got all the normal
amenities: a casino, an arcade, a kart track, and a pawn shop. We've also got
lots of lava, so no need to bring any blankets for warmth!

:::